prefix = slides

all:
	pdflatex ${prefix}.tex
	pdflatex ${prefix}.tex
clean:
	rm -f *.pdf *.out *.log *.aux *.bbl *.blg *.toc *.mtc* *.maf *.dpth *.md5 *.bcf *.xml *-blx.bib *.snm *.nav
